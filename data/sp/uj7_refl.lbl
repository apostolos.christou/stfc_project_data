PDS_VERSION_ID                     = PDS3                                     
                                                                              
RECORD_TYPE                        = "FIXED_LENGTH"                           
RECORD_BYTES                       = 22                                       
FILE_RECORDS                       = 694                                      
                                                                              
^TABLE                             = "uj7_refl.tab"                              
                                                                              
DATA_SET_ID                        = "STFC"     
PRODUCT_NAME                       = "(121514) 1999 UJ7 CCD REFLECTANCE SPECTRUM"         
PRODUCT_ID                         = "UJ7_REFL_TAB"                              
INSTRUMENT_HOST_ID                 = "WHT"                               
INSTRUMENT_HOST_NAME               = "4.2-m William Herschel Telescope"   
INSTRUMENT_ID                      = "ACAM"                                  
INSTRUMENT_NAME                    = "ACAM Auxiliary-port CAMera"                  
TARGET_NAME                        = "(121514) 1999 UJ7"                              
TARGET_TYPE                        = "ASTEROID"                               
START_TIME                         = 2017-02-17T01:52:25.177
STOP_TIME                          = 2017-02-17T02:19:07.089
PRODUCT_CREATION_TIME              = 2018-11-14
CITATION_DESC                      = "Borisov, G. et al., 2017"                                    
ABSTRACT_DESC                      = "Individual asteroid spectrum obtained with ACAM 
 		Auxiliary-port CAMera and a low-resolution spectrograph mounted on WHT 4.2-m 
 		William Herschel Telescope at The Isaac Newton Group of Telescopes (ING)."                
                                                                              
REFERENCE_KEY_ID                   = "BorisovEtAl2018"                        
RECORD_FORMAT                      = "(F6.4, 1X, F6.4, 1X, F6.4)"           
NOTE                               = "Single spectrum. For details see  
        Borisov et al., 2017 [BorisovEtAl2017]."         
                                                                              
OBJECT                             = TABLE                                    
 ROWS                              = 694                                      
 ROW_BYTES                         = 22                                       
 INTERCHANGE_FORMAT                = "ASCII"                                  
 COLUMNS                           = 3                                        
 DESCRIPTION                       = "Individual asteroid spectrum obtained with ACAM 
 		Auxiliary-port CAMera and a low-resolution spectrograph mounted on WHT 4.2-m 
 		William Herschel Telescope at The Isaac Newton Group of Telescopes (ING)."                
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 1                                        
  NAME                             = "WAVELENGTH"                             
  UNIT                             = "MICRON"                                 
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 1                                        
  BYTES                            = 6                                        
  FORMAT                           = "F6.4"                                   
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 2                                        
  NAME                             = "REFLECTANCE"                            
  DESCRIPTION                      = "This is relative reflectance normalized 
        to 1.0 at 0.55 microns."                                              
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 8                                        
  BYTES                            = 6                                        
  FORMAT                           = "F6.4"                                   
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 3                                        
  NAME                             = "UNCERTAINTY"                            
  DESCRIPTION                      = "This is a formal uncertainty       
        per wavelength channel based on Poisson statistics."      
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 15                                       
  BYTES                            = 6                                        
  FORMAT                           = "F6.4"                                   
 END_OBJECT                        = COLUMN                                   
                                                                              
END_OBJECT                         = TABLE                                    
END                                                                           
