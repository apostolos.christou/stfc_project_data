PDS_VERSION_ID                     = PDS3                                     
                                                                              
RECORD_TYPE                        = "FIXED_LENGTH"                           
RECORD_BYTES                       = 22                                       
FILE_RECORDS                       = 813                                      
                                                                              
^TABLE                             = "495_refl.tab"                              
                                                                              
DATA_SET_ID                        = "STFC"     
PRODUCT_NAME                       = "495 EULALIA CCD REFLECTANCE SPECTRUM"         
PRODUCT_ID                         = "495_REFL_TAB"                              
INSTRUMENT_HOST_ID                 = "2mRCC"                               
INSTRUMENT_HOST_NAME               = "BNAO Rozhen 2-M RCC TELESCOPE   
INSTRUMENT_ID                      = "FoReRo2"                                  
INSTRUMENT_NAME                    = "2-Channel Focal Reducer - Rozhen"                  
TARGET_NAME                        = "495 EULALIA"                              
TARGET_TYPE                        = "ASTEROID"                               
START_TIME                         = 2018-02-09T02:30:07                      
STOP_TIME                          = 2018-02-09T02:40:07                      
PRODUCT_CREATION_TIME              = 2018-11-14
CITATION_DESC                      = "Borisov, G. et al., 2018"                                    
ABSTRACT_DESC                      = "Individual asteroid spectrum obtained   
        with FoReRo2 instrument mounted on 2m RCC telescopes at BNAO Rozhen."                
                                                                              
REFERENCE_KEY_ID                   = "BorisovEtAl2018"                        
RECORD_FORMAT                      = "(F6.4, 1X, F6.4, 1X, F6.4)"           
NOTE                               = "Single spectrum. For details see  
        Borisov et al., 2018 [BorisovEtAl2018]."         
                                                                              
OBJECT                             = TABLE                                    
 ROWS                              = 813                                      
 ROW_BYTES                         = 22                                       
 INTERCHANGE_FORMAT                = "ASCII"                                  
 COLUMNS                           = 3                                        
 DESCRIPTION                       = "Individual asteroid spectrum obtained   
        with FoReRo2 instrument mounted on 2m RCC telescopes at BNAO Rozhen."                
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 1                                        
  NAME                             = "WAVELENGTH"                             
  UNIT                             = "MICRON"                                 
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 1                                        
  BYTES                            = 6                                        
  FORMAT                           = "F6.4"                                   
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 2                                        
  NAME                             = "REFLECTANCE"                            
  DESCRIPTION                      = "This is relative reflectance normalized 
        to 1.0 at 0.55 microns."                                              
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 8                                        
  BYTES                            = 6                                        
  FORMAT                           = "F6.4"                                   
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 3                                        
  NAME                             = "UNCERTAINTY"                            
  DESCRIPTION                      = "This is a formal uncertainty       
        per wavelength channel based on Poisson statistics."      
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 15                                       
  BYTES                            = 6                                        
  FORMAT                           = "F6.4"                                   
 END_OBJECT                        = COLUMN                                   
                                                                              
END_OBJECT                         = TABLE                                    
END                                                                           
