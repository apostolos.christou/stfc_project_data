PDS_VERSION_ID                     = PDS3                                     
                                                                              
RECORD_TYPE                        = "FIXED_LENGTH"                           
RECORD_BYTES                       = 68                                       
FILE_RECORDS                       = 1341                                     
                                                                              
^TABLE                             = "ccd_spectra_param_table.tab"            
                                                                              
DATA_SET_ID                        = "STFC"     
PRODUCT_NAME                       = "PARAMETER TABLE"                        
PRODUCT_ID                         = "CCD_SPECTRA_PARAM_TABLE"                
INSTRUMENT_HOST_ID                 = "N/A"                                    
INSTRUMENT_HOST_NAME               = "N/A"                                    
INSTRUMENT_ID                      = "N/A"                                    
INSTRUMENT_NAME                    = "N/A"                                    
TARGET_NAME                        = "N/A"                                    
TARGET_TYPE                        = "N/A"                                    
START_TIME                         = "N/A"                                    
STOP_TIME                          = "N/A"                                    
PRODUCT_CREATION_TIME              = 2018-11-14  /* File uploaded to OLAF */  
CITATION_DESC                      = "Borisov, G. et al., 2018."                                    
RECORD_FORMAT                      = "(A14, 1X, I2, 1X, A8, 1X, F5.3, 1X, A8, 
        1X, F9.1, 1X, F10.2, 1X, F3.114X)"                                    
                                                                              
OBJECT                             = TABLE                                    
 ROWS                              = 1                                     
 ROW_BYTES                         = 68                                       
 INTERCHANGE_FORMAT                = "ASCII"                                  
 COLUMNS                           = 8                                        
 DESCRIPTION                       = "This table contains aditional parameters
        for each spectrum in this data set. The parameters in this table are 
        values for which keywords did not exist and therefore could not be put
        into the labels of the spectra."                                       
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 1                                        
  NAME                             = "FILE"                                   
  DESCRIPTION                      = "This is the file name and path for the  
        data file that is associated with all the rest of the parameters in   
        the table row.  The path is relative to the data directory."          
  DATA_TYPE                        = "CHARACTER"                              
  START_BYTE                       = 1                                        
  BYTES                            = 14                                       
  FORMAT                           = "A14"                                    
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 2                                        
  NAME                             = "EXPOSURE_COUNT"                         
  DESCRIPTION                      = "This is the number of spectra combined  
        to form this spectrum."                                               
  DATA_TYPE                        = "ASCII_INTEGER"                          
  START_BYTE                       = 16                                       
  BYTES                            = 2                                        
  FORMAT                           = "I2"                                     
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 3                                        
  NAME                             = "INTEGRATION_TIME"                       
  DESCRIPTION                      = "This is the sum of all exposure times of
        spectra combined to form this spectrum."                              
  DATA_TYPE                        = "TIME"                                   
  START_BYTE                       = 19                                       
  BYTES                            = 8                                        
  FORMAT                           = "A8"                                     
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 4                                        
  NAME                             = "SPECTRAL_RESOLUTION"                    
  DESCRIPTION                      = "This column indicates the               
        wavelength/frequency resolution of this spectrum."                    
  UNIT                             = "MICRON"                                 
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 28                                       
  BYTES                            = 6                                        
  FORMAT                           = "F6.4"                                   
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 5                                        
  NAME                             = "STAR_NAME"                              
  DESCRIPTION                      = "This is the name of the solar analog    
        star used in the data reduction."                                     
  DATA_TYPE                        = "CHARACTER"                              
  START_BYTE                       = 35                                       
  BYTES                            = 8                                        
  FORMAT                           = "A8"                                     
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 6                                        
  NAME                             = "VISUAL_MAGNITUDE"                       
  DESCRIPTION                      = "This is the apparent visual magnitude of
        the target at the time of observation."                               
  UNIT                             = "MAGNITUDE"                              
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 43                                       
  BYTES                            = 9                                        
  FORMAT                           = "F9.1"                                   
  MISSING_CONSTANT                 = -999999.9                                
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 7                                        
  NAME                             = "HELIOCENTRIC_DISTANCE"                  
  DESCRIPTION                      = "The distance from the target to the sun 
        in AU at the time of observation."                                    
  UNIT                             = "ASTRONOMICAL UNIT"                      
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 53                                       
  BYTES                            = 10                                       
  FORMAT                           = "F10.2"                                  
  MISSING_CONSTANT                 = -999999.9                                
 END_OBJECT                        = COLUMN                                   
                                                                              
 OBJECT                            = COLUMN                                   
  COLUMN_NUMBER                    = 8                                        
  NAME                             = "SLIT_WIDTH"                             
  DESCRIPTION                      = "This is the width in arcseconds of the  
        dispersing slit of the spectrometer."                                 
  UNIT                             = "ARCSECONDS"                             
  DATA_TYPE                        = "ASCII_REAL"                             
  START_BYTE                       = 64                                       
  BYTES                            = 3                                        
  FORMAT                           = "F3.1"                                   
 END_OBJECT                        = COLUMN                                   
                                                                              
END_OBJECT                         = TABLE                                    
END                                                                           
