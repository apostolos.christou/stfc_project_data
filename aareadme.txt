PDS_VERSION_ID                     = PDS3                                     
RECORD_TYPE                        = STREAM                                   
                                                                              
OBJECT                             = TEXT                                     
   PUBLICATION_DATE                = 2018-11-14  /* Label Last Generated */   
   NOTE                            = "Volume information"                     
END_OBJECT                         = TEXT                                     
                                                                              
END                                                                           
                                                                              
PDS OLAF Data Set Volume                                                      
                                                                              
Peer Review: xxxxxxxxxx                                  
Discipline Node: Small Bodies                                                 
Peer Review Contact: xxxxxxxxxx
                                                                              
Data Set Name: STFC Martian Trojans                                                       
                                                                              
The data and associated PDS labels and catalog files are organized into the   
        following directories:                                                
                                                                              
catalog - containing PDS catalog files for the data set, observatories,       
        instruments, facilities, and a listing of references.                 
                                                                              
data - containing the data files and their corresponding labels.              
                                                                              
index - containing the PDS required index and its label.                      
